/*

в циклах не != а <

в evaluator row и col могут быть перепутаны

*/

#include <random>
#include <thread>
#include <atomic>
#include <array>
#include <mutex>
#include <condition_variable>

#ifdef ENABLE_SSE
#include <xmmintrin.h>
#include <emmintrin.h>
#endif

#include "matrix.h"

#ifdef ENABLE_AVX
#include <xmmintrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#endif

matrix::matrix(size_t n) {
	_data.resize(n);
	for (auto & elem : _data) {
		elem.resize(n, 0.);
	}
	_size = n;
}

void matrix::randomize() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<double> uid(-1., 0.);
	for (auto & row: _data) {
		for (auto & elem: row) {
			elem = -uid(gen);
		}
	}

}

std::ostream& operator<< (std::ostream& out, const matrix& m) {
	bool first_row = true;
	out << "{";
	for (const auto &row : m._data) {
		if (first_row) {
			first_row = false;
		} else {
			out << "," << std::endl;
		}
		bool first_elem = true;
		out << "{";
		for (const auto &elem : row) {
			if (first_elem) {
				first_elem = false;
			} else {
				out << "; ";
			}
			out << elem;
		}
		out << "}";
	}
	out << "}";
	return out;
}

std::istream& operator>> (std::istream &in, matrix& m) {
	char c;
	in >> c;
	assert(c == '{');
	bool first_row = true;
	for (size_t row = 0; row < m.size(); ++row) {
		
		if (first_row) {
			first_row = false;
		} else {
			in >> c;
			assert(c == ',');
		}
		in >> c;
		assert(c == '{');
		bool first_elem = true;
		for (size_t col = 0; col < m.size(); ++col) {
			if (first_elem) {
				first_elem = false;
			} else {
				in >> c;
				assert(c == ',');
			}
			in >> m._data[row][col];

		}
		in >> c;
		assert(c == '}');
	}
	in >> c;
	assert(c == '}');
	return in;
}


void evaluate_scalar_product(matrix& m, const matrix& m1, const matrix& m2tr,
	size_t row, size_t col) {
		double sum_ = 0;
		size_t size = m.size();
#ifdef ENABLE_SSE
		__m128d prod, sum;
		auto p1 = reinterpret_cast<const __m128d*>(m1._data[row].data());
		auto p2 = reinterpret_cast<const __m128d*>(m2tr._data[col].data());
		sum = _mm_sub_pd(sum,sum);
		for (size_t run = 0; run != m1.size(); run +=2) {
			prod = _mm_mul_pd(*p1, *p2);
			sum = _mm_add_pd(sum, prod);
			++p1;
			++p2;
		}
		double sumd[2];
		_mm_store_pd(sumd, sum);
		m._data[row][col] = sumd[0] + sumd[1];
#elif defined(ENABLE_AVX)
		__m256d prod, sum;
		auto p1 = reinterpret_cast<const __m256d*>(m1._data[row].data());
		auto p2 = reinterpret_cast<const __m256d*>(m2tr._data[col].data());
		sum = _mm256_sub_pd(sum,sum);
		for (size_t run = 0; run != m1.size(); run +=4) {
			prod = _mm256_mul_pd(*p1, *p2);
			sum = _mm256_add_pd(sum, prod);
			++p1;
			++p2;
		}
		double sumd[4];
		_mm256_store_pd(sumd,sum);
		m._data[row][col] = sumd[0] + sumd[1] + sumd[2] + sumd[3];

#else	
		double prod, sum;
		const double* p1 = m1._data[row].data();
		const double* p2 = m2tr._data[col].data();
		sum = sum - sum;
		for (size_t run = 0; run != m1.size(); run +=2) {
			prod = *p1 * *p2;
			sum += prod;
			++p1;
			++p2;
		}
		m._data[row][col] = sum;
		// for(size_t k = 0; k < size ; k++) {
		//	sum_ += m1._data[row][k]*m2tr._data[col][k];
		// m._data[row][col]  = sum_;
#endif
}

#define NTHREADS 8

#ifdef ENABLE_THREADS

	void product_evaluator(matrix& m, const matrix& m1, const matrix& m2tr,
		size_t shift) {
			size_t size = m.size();
			for (size_t i = shift; i < size; i += NTHREADS) {
				for (size_t j = 0; j < size; j++) {
					evaluate_scalar_product(m, m1, m2tr, i, j);
				}
			}
	} 

	void evaluation_thread(matrix& m, const matrix& m1, const matrix& m2tr, 
		size_t &shared_row, std::mutex &mut) {
			size_t size = m.size();
			while (true) {
				mut.lock();
				size_t row = shared_row++;
				mut.unlock();
				if (row >= size) {
					break;
				} 
				for (size_t j = 0; j < size; j++) {
					evaluate_scalar_product(m, m1, m2tr, row, j);
				}

			}
	}



#endif

matrix operator*(const matrix& m1, const matrix& m2) {
	assert(m1.size() == m2.size());
	size_t size = m1.size();
	matrix m(size);
	matrix m2tr = transpose(m2);

#ifdef ENABLE_THREADS
	std::array<std::thread, NTHREADS> threads;
	size_t shared_row = {};
	std::mutex mut;
	for (size_t shift = 0; shift < NTHREADS; ++shift) {
		threads[shift] = std::thread(evaluation_thread, std::ref(m),
	std::cref(m1), std::cref(m2tr), std::ref(shared_row), std::ref(mut));
	}
	for (size_t shift = 0; shift < NTHREADS; ++shift)
		threads[shift].join();

#else
#ifdef ENABLE_OPENMP
#pragma omp parallel for
#endif

	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			evaluate_scalar_product(m, m1, m2tr, i, j);
		}
	}
#endif

	return m;	
}

bool operator== (const matrix& m1, const matrix& m2) {
	if (m1.size() != m2.size()) {
		return false;
	}
	size_t size = m1.size();

	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			if(fabs(m1._data[i][j] - m1._data[i][j]) >= 0.000001)
				return false;
		}
	}

	return true;
	
}

matrix transpose(const matrix& m) {
	size_t size = m.size();
	matrix result(size);
	for (size_t i = 0; i < size ; i++) {
		for (size_t j = 0; j < size ; j++) {
			result._data[i][j] = m._data[j][i];
		}
	}

	return result;
}
