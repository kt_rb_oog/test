#include <iostream>
#include <fstream>
#include <chrono>

#include "matrix.h"

#ifdef ENABLE_MPI

#include "mpi.h"

void run_mpi_slave(int rank) {
	MPI_Finalize();

}

#endif

int main(int argc, char* argv[]) {
	
#ifdef ENABLE_MPI 
	MPI_Init(&argc, &argv);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	if (world_size == 1) {
		std::cout << "Should have at least 2 processes\n";
		MPI_Finalize();
		return 1;
	}
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank != 0) {
		run_mpi_slave(rank);
		return 0;
	}
#endif


	if (argc == 1) {
		matrix a(30);
		std::ifstream in("tests/matrix.in");
		in >> a;
		matrix b(30);
		std::ifstream inout("tests/matrix.out");
		inout >> b;
		if (a*a == b) {
			std::cout << "Test passed" << std::endl;
#ifdef ENABLE_MPI
			MPI_Finalize();
#endif
			return 0;
		} else {
			std::cout << "Test failed" << std::endl;
			std::cout << a*a << std::endl;
			std::cout << b << std::endl;
#ifdef ENABLE_MPI
			MPI_Finalize();
#endif
			return 1;
		}
	} else {
		int n;
		if (!sscanf(argv[1], "%d", &n)) {
			std::cout << "Cannot read n\n";
			return 1;
		}
		matrix a(n);
		a.randomize();
		matrix b(n);
		b.randomize();
		auto start = std::chrono::steady_clock::now();
		matrix c(a*b);
		auto end = std::chrono::steady_clock::now();
		auto diff = end - start;
		int time = std::chrono::duration_cast<std::chrono::milliseconds>
			(diff).count();
		std::cout << time << " milliseconds " << std::endl;
	}
#ifdef ENABLE_MPI
	MPI_Finalize();
#endif
	return 0;
} 