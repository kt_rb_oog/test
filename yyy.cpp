#include <stdio.h>

int main() {

	double num = 42;
	double num2 = 42;
	size_t addr = reinterpret_cast<size_t>(&num);
	size_t addr2 = reinterpret_cast<size_t>(&num2);
	printf("%zx\n", addr);
	printf("%zx\n", addr2);
}
